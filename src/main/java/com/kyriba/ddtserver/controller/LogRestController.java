package com.kyriba.ddtserver.controller;

import com.kyriba.ddtserver.provider.LogsProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;


@RestController
public class LogRestController {

    private final LogsProvider logsProvider;

    @Autowired
    public LogRestController(LogsProvider logsProvider) {
        this.logsProvider = logsProvider;
    }


    @GetMapping("/v1/logs")
    public @ResponseBody ResponseEntity<Resource> downloadLogs() {
        byte[] data = new byte[0];
        try {
            data = logsProvider.getLogs().toByteArray();
            ByteArrayResource resource = new ByteArrayResource(data);
            HttpHeaders headers = new HttpHeaders();
            headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=logs.zip");
            return ResponseEntity.ok()
                    .headers(headers)
                    .contentLength(data.length)
                    .contentType(MediaType.APPLICATION_OCTET_STREAM) //todo +ZIP
                    .body(resource);
        } catch (IOException exception) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }


    @DeleteMapping("/v1/logs")
    public @ResponseBody ResponseEntity<String> deleteLogs() {
        try {
            logsProvider.cleanLogsDirectory();
            return new ResponseEntity<>("All logs were deleted", HttpStatus.OK);
        } catch (IOException exception) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }
}

