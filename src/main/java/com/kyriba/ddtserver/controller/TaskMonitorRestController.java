package com.kyriba.ddtserver.controller;

import com.kyriba.ddtserver.provider.TaskMonitorProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class TaskMonitorRestController {

    private final TaskMonitorProvider taskMonitorProvider;

    @Autowired
    public TaskMonitorRestController(TaskMonitorProvider taskMonitorProvider) {
        this.taskMonitorProvider = taskMonitorProvider;
    }

    @DeleteMapping("/v1/taskmonitor")
    public @ResponseBody ResponseEntity<String> clearTaskMonitor() {
       taskMonitorProvider.cleanTaskMonitor();
       return new ResponseEntity<>("Task monitor history was deleted", HttpStatus.OK);
    }
}

