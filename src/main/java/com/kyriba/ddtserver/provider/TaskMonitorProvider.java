/********************************************************************************
 * Copyright 2021 Kyriba Corp. All Rights Reserved.                             *
 * The content of this file is copyrighted by Kyriba Corporation and can not be *
 * reproduced, distributed, altered or used in any form, in whole or in part.   *
 *******************************************************************************/
package com.kyriba.ddtserver.provider;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;


/**
 * @author M-KAV
 */
@Repository
public class TaskMonitorProvider {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public TaskMonitorProvider(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void cleanTaskMonitor() {
        jdbcTemplate.update("UPDATE TASK_MONITOR_LINE SET IS_DELETED=1");
    }
}